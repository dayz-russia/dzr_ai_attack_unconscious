modded class PlayerBase
{
	override bool CanBeTargetedByAI(EntityAI ai)
	{
		return true;
	};
	
	override void OnUnconsciousStart()
	{		
		super.OnUnconsciousStart();
		if ( GetInstanceType() == DayZPlayerInstanceType.INSTANCETYPE_CLIENT )
		{
			GetGame().GetSoundScene().SetSoundVolume(0.15,3);
		};
	};
	
	override void OnUnconsciousUpdate(float pDt, int last_command)
	{
		super.OnUnconsciousUpdate(pDt, last_command);
		if( GetGame().IsClient() || !GetGame().IsMultiplayer() )
		{
			if(GetPulseType() == EPulseType.REGULAR)
			{
				PPEffects.SetTunnelVignette(0.93);
				PPEffects.SetUnconsciousnessVignette(0.93);
			};
		};
	};	
	void OnUnconsciousStop(int pCurrentCommandID)
	{	
		super.OnUnconsciousStop(pCurrentCommandID);
		if( GetInstanceType() == DayZPlayerInstanceType.INSTANCETYPE_CLIENT ) 
		{
			if ( pCurrentCommandID != DayZPlayerConstants.COMMANDID_DEATH )
			{
				PPEffects.ResetAll();
			};
		};
	};
};
