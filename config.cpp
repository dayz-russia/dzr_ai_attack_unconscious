class CfgPatches
{
class dzr_ai_attack_unconscious
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"DZ_Data"};
	};
};
class CfgMods
{
	class dzr_ai_attack_unconscious
	{
		dir = "dzr_ai_attack_unconscious";
		picture = "";
		action = "";
		hideName = 1;
		hidePicture = 1;
		name = "dzr_ai_attack_unconscious";
		credits = "DayzRussia.com";
		author = "DayzRussia.com";
		authorID = "0";
		version = "0.1";
		extra = 0;
		type = "mod";
		dependencies[] = {"Game","World","Mission"};
		class defs
		{
			class worldScriptModule
			{
				value = "";
				files[] = {"dzr_ai_attack_unconscious/scripts/4_World"};
			};
		};
	};
};
